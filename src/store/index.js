import { createStore } from 'vuex'
import Servicio from "@/core/servicio/Servicio";
import Ruta from "@/core/servicio/Ruta";
import router from "@/router";

export default createStore({
  state: {
    usuario: {
      nombreCompleto: '',
      telefono: '',
      carnetIdentidad: '',
      urlFoto: "",
      correo: ''
    },
    autenticado: false
  },
  getters: {
    getUsuario(state) {
      return state.usuario;
    },
    isAutenticado(state) {
      return state.autenticado;
    }
  },
  mutations: {
    SET_USUARIO(state, usuario) {
      if(usuario != null) {
        state.usuario = usuario;
        state.autenticado = true;
      }
    }
  },
  actions: {
    obtenerUsuarioAutenticado({ commit }) {
      Servicio.get(Ruta.USUARIO_AUTENTICADO).then( (data) => {
        if(router.currentRoute.value.path === '/') {
          router.push('/inicio');
        }
        commit('SET_USUARIO', data.data);
      }).catch( () => {
        router.push('/login');
        commit('SET_USUARIO', null);
      });
    }
  },
  modules: {
  }
})
