export default class LoginNegocio {

    static iniciarProgressBtnLogin() {
        const btnLogin = document.querySelector("#btnLogin");
        btnLogin.setAttribute("disabled", "true");
        btnLogin.setAttribute("data-kt-indicator", "on");
    }

    static terminarProgressBtnLogin() {
        const btnLogin = document.querySelector("#btnLogin");
        btnLogin.removeAttribute("data-kt-indicator");
        btnLogin.removeAttribute("disabled");
    }
}