export default class RegistrarNegocio {
    constructor() {
        this.bloqueFormulario = document.querySelector("#formulario");

        this.bloqueFormularioUI = new window.KTBlockUI(this.bloqueFormulario, {
            message: '<div class="blockui-message"><span class="spinner-border text-primary"></span> Espere un momento por favor...</div>',
        });
    }

    iniciarBloqueoFormulario() {
        if (!this.bloqueFormularioUI.isBlocked()) {
            this.bloqueFormularioUI.block();
        }
    }

    terminarBloqueoFormulario() {
        if (this.bloqueFormularioUI.isBlocked()) {
            this.bloqueFormularioUI.release();
        }
    }

}