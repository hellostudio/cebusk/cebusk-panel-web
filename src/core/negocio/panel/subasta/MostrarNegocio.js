export default class MostrarNegocio {
    constructor() {
        this.bloquePrincipal = document.querySelector("#bloquePrincipal");

        this.bloquePrincipalUI = new window.KTBlockUI(this.bloquePrincipal, {
            message: '<div class="blockui-message"><span class="spinner-border text-primary"></span> Espere un momento por favor...</div>',
        });
    }

    iniciarBloqueoPrincipal() {
        if (!this.bloquePrincipalUI.isBlocked()) {
            this.bloquePrincipalUI.block();
        }
    }

    terminarBloqueoPrincipal() {
        if (this.bloquePrincipalUI.isBlocked()) {
            this.bloquePrincipalUI.release();
        }
    }

    destructor() {
        this.bloquePrincipalUI.destroy();
    }
}