export default class Ruta {
    //static DOMINIO = process.env.VUE_APP_API_URL;
    static DOMINIO = 'https://core.cebus-k.com';


    static AUTENTICACION_LOGIN = Ruta.DOMINIO + '/api/panel/login';
    static USUARIO_AUTENTICADO = Ruta.DOMINIO + '/api/panel/user/autenticado';
    static AUTENTICACION_CERRAR_SESION = Ruta.DOMINIO + '/api/panel/cerrar-sesion';

    static PANEL_SUBASTA_LISTAR = Ruta.DOMINIO + '/api/panel/subasta/listar';
    static PANEL_SUBASTA_BUSCAR = Ruta.DOMINIO + '/api/panel/subasta/buscar';
    static PANEL_SUBASTA_REGISTRAR = Ruta.DOMINIO + '/api/panel/subasta/registrar';
    static rutaPanelSubastaMostrar(subastaId) {
        return Ruta.DOMINIO + '/api/panel/subasta/' + subastaId + '/mostrar';
    }
    static rutaPanelSubastaModificar(subastaId) {
        return Ruta.DOMINIO + '/api/panel/subasta/' + subastaId + '/modificar';
    }
    static rutaPanelSubastaEliminar(subastaId) {
        return Ruta.DOMINIO + '/api/panel/subasta/' + subastaId + '/eliminar';
    }

    static rutaPanelSubastasGaleriaListar(subastaId) {
        return Ruta.DOMINIO + '/api/panel/subasta/' + subastaId + '/galeria/listar';
    }
    static rutaPanelSubastasGaleriaRegistrar(subastaId) {
        return Ruta.DOMINIO + '/api/panel/subasta/' + subastaId + '/galeria/registrar';
    }
    static rutaPanelSubastasGaleriaEliminar(subastaId, id) {
        return Ruta.DOMINIO + '/api/panel/subasta/' + subastaId + '/galeria/'+ id + '/eliminar';
    }


    //---------------------------- API: Tipo de Ganado ------------------------------------
    static PANEL_TIPO_GANADO_LISTAR = Ruta.DOMINIO + '/api/panel/tipo-ganado/listar';
    static PANEL_TIPO_GANADO_REGISTRAR = Ruta.DOMINIO + '/api/panel/tipo-ganado/registrar';
    static rutaPanelTipoGanadoMostrar(tipoGanadoId) {
        return Ruta.DOMINIO + '/api/panel/tipo-ganado/'+tipoGanadoId+'/mostrar';
    }
    static rutaPanelTipoGanadoModificar(tipoGanadoId) {
        return Ruta.DOMINIO + '/api/panel/tipo-ganado/'+tipoGanadoId+'/modificar';
    }
    static rutaPanelTipoGanadoEliminar(tipoGanadoId) {
        return Ruta.DOMINIO + '/api/panel/tipo-ganado/'+tipoGanadoId+'/eliminar';
    }
    //---------------------------- API FIN: Tipo de Ganado ---------------------------------

    //---------------------------- API: Tipo de Raza ------------------------------------
    static PANEL_TIPO_RAZA_LISTAR = Ruta.DOMINIO + '/api/panel/tipo-raza/listar';
    static PANEL_TIPO_RAZA_REGISTRAR = Ruta.DOMINIO + '/api/panel/tipo-raza/registrar';
    static rutaPanelTipoRazaMostrar(tipoRazaId) {
        return Ruta.DOMINIO + '/api/panel/tipo-raza/'+tipoRazaId+'/mostrar';
    }
    static rutaPanelTipoRazaModificar(tipoRazaId) {
        return Ruta.DOMINIO + '/api/panel/tipo-raza/'+tipoRazaId+'/modificar';
    }
    static rutaPanelTipoRazaEliminar(tipoRazaId) {
        return Ruta.DOMINIO + '/api/panel/tipo-raza/'+tipoRazaId+'/eliminar';
    }
    //---------------------------- API FIN: Tipo de Raza ---------------------------------

    //---------------------------- API: Tipo de Animal ------------------------------------
    static PANEL_TIPO_ANIMAL_LISTAR = Ruta.DOMINIO + '/api/panel/tipo-animal/listar';
    static PANEL_TIPO_ANIMAL_REGISTRAR = Ruta.DOMINIO + '/api/panel/tipo-animal/registrar';
    static rutaPanelTipoAnimalMostrar(tipoAnimalId) {
        return Ruta.DOMINIO + '/api/panel/tipo-animal/'+tipoAnimalId+'/mostrar';
    }
    static rutaPanelTipoAnimalModificar(tipoAnimalId) {
        return Ruta.DOMINIO + '/api/panel/tipo-animal/'+tipoAnimalId+'/modificar';
    }
    static rutaPanelTipoAnimalEliminar(tipoAnimalId) {
        return Ruta.DOMINIO + '/api/panel/tipo-animal/'+tipoAnimalId+'/eliminar';
    }
    //---------------------------- API FIN: Tipo de Animal ---------------------------------

    //---------------------------- API: Categoria de Ganado ------------------------------------
    static PANEL_CATEGORIA_GANADO_LISTAR = Ruta.DOMINIO + '/api/panel/categoria-ganado/listar';
    static PANEL_CATEGORIA_GANADO_REGISTRAR = Ruta.DOMINIO + '/api/panel/categoria-ganado/registrar';
    static rutaPanelCategoriaGanadoMostrar(categoriaGanadoId) {
        return Ruta.DOMINIO + '/api/panel/categoria-ganado/'+categoriaGanadoId+'/mostrar';
    }
    static rutaPanelCategoriaGanadoModificar(categoriaGanadoId) {
        return Ruta.DOMINIO + '/api/panel/categoria-ganado/'+categoriaGanadoId+'/modificar';
    }
    static rutaPanelCategoriaGanadoEliminar(categoriaGanadoId) {
        return Ruta.DOMINIO + '/api/panel/categoria-ganado/'+categoriaGanadoId+'/eliminar';
    }
    //---------------------------- API FIN: Categoria de Ganado ---------------------------------

    //---------------------------- API: Asociacion ------------------------------------
    static PANEL_ASOCIACION_LISTAR = Ruta.DOMINIO + '/api/panel/asociacion/listar';
    static PANEL_ASOCIACION_REGISTRAR = Ruta.DOMINIO + '/api/panel/asociacion/registrar';
    static rutaPanelAsociacionMostrar(asociacionId) {
        return Ruta.DOMINIO + '/api/panel/asociacion/'+asociacionId+'/mostrar';
    }
    static rutaPanelAsociacionModificar(asociacionId) {
        return Ruta.DOMINIO + '/api/panel/asociacion/'+asociacionId+'/modificar';
    }
    static rutaPanelAsociacionEliminar(asociacionId) {
        return Ruta.DOMINIO + '/api/panel/asociacion/'+asociacionId+'/eliminar';
    }
    //---------------------------- API FIN: Asociacion ---------------------------------

    //---------------------------- API: Proveedor ------------------------------------
    static PANEL_PROVEEDOR_LISTAR = Ruta.DOMINIO + '/api/panel/proveedor/listar';
    static PANEL_PROVEEDOR_REGISTRAR = Ruta.DOMINIO + '/api/panel/proveedor/registrar';
    static rutaPanelProveedorMostrar(proveedorId) {
        return Ruta.DOMINIO + '/api/panel/proveedor/'+proveedorId+'/mostrar';
    }
    static rutaPanelProveedorModificar(proveedorId) {
        return Ruta.DOMINIO + '/api/panel/proveedor/'+proveedorId+'/modificar';
    }
    static rutaPanelProveedorEliminar(proveedorId) {
        return Ruta.DOMINIO + '/api/panel/proveedor/'+proveedorId+'/eliminar';
    }
    //---------------------------- API FIN: Proveedor ---------------------------------

    //---------------------------- API: Producto ------------------------------------
    static rutaPanelProductoListar(proveedorId) {
        return Ruta.DOMINIO + '/api/panel/proveedor/'+proveedorId+'/producto/listar';
    }
    static rutaPanelProductoRegistrar(proveedorId) {
        return Ruta.DOMINIO + '/api/panel/proveedor/'+proveedorId+'/producto/registrar';
    }
    static rutaPanelProductoMostrar(proveedorId, productoId) {
        return Ruta.DOMINIO + '/api/panel/proveedor/'+proveedorId+'/producto/'+productoId+'/mostrar';
    }
    static rutaPanelProductoModificar(proveedorId, productoId) {
        return Ruta.DOMINIO + '/api/panel/proveedor/'+proveedorId+'/producto/'+productoId+'/modificar';
    }
    static rutaPanelProductoEliminar(proveedorId, productoId) {
        return Ruta.DOMINIO + '/api/panel/proveedor/'+proveedorId+'/producto/'+productoId+'/eliminar';
    }
    //---------------------------- API FIN: Producto ---------------------------------

    //---------------------------- API: Venta ------------------------------------
    static PANEL_PUBLICACION_LISTAR = Ruta.DOMINIO + '/api/panel/publicacion/listar';
    static PANEL_PUBLICACION_BUSCAR = Ruta.DOMINIO + '/api/panel/publicacion/buscar';
    static PANEL_PUBLICACION_FORMULARIO_BUSCAR = Ruta.DOMINIO + '/api/panel/publicacion/buscar';
    static rutaPanelPublicacionMostrar(publicacionId) {
        return Ruta.DOMINIO + '/api/panel/publicacion/' + publicacionId + '/mostrar';
    }
    static rutaPanelPublicacionGaleriaListar(publicacionId) {
        return Ruta.DOMINIO + '/api/panel/publicacion/' + publicacionId + '/galeria/listar';
    }
    //---------------------------- API FIN: Venta ---------------------------------

    //---------------------------- API: Transporte ------------------------------------
    static PANEL_TRANSPORTE_LISTAR = Ruta.DOMINIO + '/api/panel/transporte/listar';
    static PANEL_TRANSPORTE_BUSCAR = Ruta.DOMINIO + '/api/panel/transporte/buscar';
    static rutaPanelTransporteMostrar(transporteId) {
        return Ruta.DOMINIO + '/api/panel/transporte/' + transporteId + '/mostrar';
    }
    //---------------------------- API FIN: Transporte ---------------------------------

    //---------------------------- API: Usuario ------------------------------------
    static PANEL_USUARIO_LISTAR = Ruta.DOMINIO + '/api/panel/usuario/listar';
    static rutaPanelUsuarioMostrar(usuarioId) {
        return Ruta.DOMINIO + '/api/panel/usuario/' + usuarioId + '/mostrar';
    }
    static rutaPanelUsuarioPublicacionListar(usuarioId) {
        return Ruta.DOMINIO + '/api/panel/usuario/' + usuarioId + '/publicacion/listar';
    }
    //---------------------------- API FIN: Usuario ---------------------------------
}