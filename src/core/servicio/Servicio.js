import router from "@/router";

export default class Servicio {

    static post(ruta, dato) {
        const configuracion = {
            method: "POST",
            headers: {
                'Content-Type': 'application/json; charset=UTF-8',
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('token')
            },
            body: JSON.stringify(dato)
        };
        return new Promise( (resolve, reject) => {
            fetch(ruta,  configuracion)
                .then(async response => {
                    const respuesta = await response.json();
                    if (!response.ok || response.status === 202) {
                        if(response.status === 401) {
                            router.push('/login');
                            reject('Usuario no autenticado, vuelva a iniciar sesión');
                        } else {
                            console.log(respuesta);
                            const error = (respuesta && respuesta.mensaje) || response.statusText;
                            reject(error);
                        }
                    }
                    resolve(respuesta);
                }).catch( error => {
                    console.log(error);
                    reject(error);
                });
        } );
    }

    static postMultiPart(ruta, dato) {
        let data = new FormData();
        for (let k of Reflect.ownKeys(dato)) {
            if (dato[k] instanceof Array) {
                for (let d in dato[k]) {
                    data.append(k + '[]', dato[k][d]);
                }
            } else {
                data.append(k, dato[ k ])
            }
        }

        const configuracion = {
            method: "POST",
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                'Accept': 'application/json'
            },
            body: data
        };

        return new Promise( (resolve, reject) => {
            fetch(ruta,  configuracion)
                .then(async response => {
                    const respuesta = await response.json();
                    if (!response.ok || response.status === 202) {
                        if(response.status === 401) {
                            router.push('/login');
                            reject('Usuario no autenticado, vuelva a iniciar sesión');
                        } else {
                            console.log(respuesta);
                            const error = (respuesta && respuesta.mensaje) || response.statusText;
                            reject(error);
                        }
                    }
                    resolve(respuesta);
                }).catch( error => {
                    console.log(error);
                    reject(error);
                });
        } );
    }


    static get(ruta) {
        const configuracion = {
            method: 'GET',
            headers: { 'Authorization': 'Bearer ' + localStorage.getItem('token'),
                'Accept': 'application/json'
            },
        }
        return new Promise( (resolve, reject) => {
            fetch(ruta, configuracion)
                .then(async response => {
                    const respuesta = await response.json();
                    if (!response.ok || response.status === 202) {
                        if(response.status === 401) {
                            router.push('/login');
                            reject('Usuario no autenticado, vuelva a iniciar sesión');
                        } else {
                            console.log(respuesta);
                            const error = (respuesta && respuesta.mensaje) || response.statusText;
                            reject(error);
                        }
                    }
                    resolve(respuesta);
                })
                .catch(error => {
                    console.log(error);
                    reject(error);
                });
        } );
    }

    static put(ruta, dato) {
        const configuracion = {
            method: "PUT",
            headers: { 'Content-Type': 'application/json; charset=UTF-8',
                'Accept': 'application/json'
            },
            body: JSON.stringify(dato)
        };
        return new Promise( (resolve, reject) => {
            fetch(ruta, configuracion)
                .then(async response => {
                    const respuesta = await response.json();
                    if (!response.ok || response.status === 202) {
                        if(response.status === 401) {
                            router.push('/login');
                            reject('Usuario no autenticado, vuelva a iniciar sesión');
                        } else {
                            console.log(respuesta);
                            const error = (respuesta && respuesta.mensaje) || response.statusText;
                            reject(error);
                        }
                    }
                    resolve(respuesta);
                }).catch( error => {
                    console.log(error);
                    reject(error);
                });
        } );
    }

    static delete(ruta) {
        const configuracion = {
            method: 'DELETE',
            headers: { 'Authorization': 'Bearer ' + localStorage.getItem('token'),
                'Accept': 'application/json'
            }
        }
        return new Promise( (resolve, reject) => {
            fetch(ruta, configuracion)
                .then(async response => {
                    const respuesta = await response.json();
                    if (!response.ok || response.status === 202) {
                        if(response.status === 401) {
                            router.push('/login');
                            reject('Usuario no autenticado, vuelva a iniciar sesión');
                        } else {
                            console.log(respuesta);
                            const error = (respuesta && respuesta.mensaje) || response.statusText;
                            reject(error);
                        }
                    }
                    resolve(respuesta);
                })
                .catch(error => {
                    console.log(error);
                    reject(error);
                });
        } );
    }
}