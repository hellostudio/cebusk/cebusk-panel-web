export default class Convertidor {

    static arrayObjectToArray(arr, key) {
        let nuevoArr = [];
        let pos = 0;
        while (pos < arr.length) {
            let obj = arr[pos];
            nuevoArr.push(obj[key]);
            pos++;
        }
        return nuevoArr;
    }
}