export default class InputMultiArchivo {

    static archivos = [];

    constructor(id, tipoFiles = 'image/*') {
        InputMultiArchivo.archivos = [];

        this.dropzoneJs = new window.Dropzone(id, {
            url: "#",
            paramName: "imagenes",
            maxFiles: 5,
            maxFilesize: 10, // MB
            acceptedFiles: tipoFiles,
            addRemoveLinks: true,
            accept: function(file, done) {
                InputMultiArchivo.archivos.push(file);
                done();
            },
            removedfile: function (file) {
                InputMultiArchivo.archivos = InputMultiArchivo.archivos.filter((f) => f.name !== file.name);
                if (file.previewElement != null && file.previewElement.parentNode != null) {
                    file.previewElement.parentNode.removeChild(file.previewElement);
                }
            }
        });
    }

    get archivos() {
        return InputMultiArchivo.archivos;
    }

    eliminarArchivos() {
        this.dropzoneJs.removeAllFiles();
    }
}