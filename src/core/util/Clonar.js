export default class Clonar {

    static clon(objeto) {
        let resultado;
        if (objeto instanceof Array) {
            resultado = [ ...objeto ];
        } else if (typeof objeto === 'object') {
            resultado = {...objeto}
        } else {
            return objeto;
        }
        for (let prop of Reflect.ownKeys (resultado)) {
            resultado[ prop ] = Clonar.clon(resultado[ prop ]);
        }
        return resultado;
    }
}