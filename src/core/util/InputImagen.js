export default class InputImagen {

    constructor(id) {
        this.id = id;
        let elemento = document.querySelector(id);
        this.ktImageInput = new window.KTImageInput(elemento);
    }

    get imagen() {
        return this.ktImageInput.getInputElement().files[0];
    }

    limpiar() {
        document.querySelector(this.id + '_eliminar').click();
    }
}