export default class Fecha {

    static getFecha() {
        let dd = new Date();
        let y = dd.getFullYear();
        let m = dd.getMonth() + 1;
        let d = dd.getDate();
        m = m < 10 ? "0" + m: m;
        d = d < 10 ? "0" + d: d;
        return y + "/" + m + "/" + d;
    }

    static getDia() {
        let dd = new Date();
        let d = dd.getDate();
        d = d < 10 ? "0" + d: d;
        return d;
    }

    static getMes() {
        let dd = new Date();
        return dd.getMonth() + 1;
    }

    static getAno() {
        let dd = new Date();
        return dd.getFullYear();
    }

    static getMesLiteral() {
        let dd = new Date();
        let m = dd.getMonth() + 1;
        switch (m) {
            case 1: return "Enero";
            case 2: return "Febrero";
            case 3: return "Marzo";
            case 4: return "Abril";
            case 5: return "Mayo";
            case 6: return "Junio";
            case 7: return "Julio";
            case 8: return "Agosto";
            case 9: return "Septiembre";
            case 10: return "Octubre";
            case 11: return "Noviembre";
            case 12: return "Diciembre";
            default: return "";
        }
    }
}