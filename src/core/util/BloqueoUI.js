export default class BloqueoUI {

    constructor(formulario = '#formulario', boton = '#btnGuardar') {
        this.bloqueFormulario = document.querySelector(formulario);
        this.bloqueFormularioUI = new window.KTBlockUI(this.bloqueFormulario, {
            message: '<div class="blockui-message"><span class="spinner-border text-primary"></span></div>',
        });
        this.btnGuardar = document.querySelector(boton);
    }

    iniciarBloqueoFormulario() {
        if (!this.bloqueFormularioUI.isBlocked()) {
            this.btnGuardar.setAttribute("data-kt-indicator", "on");
            this.bloqueFormularioUI.block();
        }
    }

    terminarBloqueoFormulario() {
        if (this.bloqueFormularioUI.isBlocked()) {
            this.bloqueFormularioUI.release();
            this.btnGuardar.removeAttribute("data-kt-indicator");
        }
    }

}