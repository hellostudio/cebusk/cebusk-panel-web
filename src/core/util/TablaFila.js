export default class TablaFila {

    /**
     *
     * @param filas {*[]}
     * @param pagina : int
     * @param cantidad : int
     * @returns {*[]}
     */
    static getFilasPagina(filas, pagina, cantidad) {
        const nuevasFilas = [];
        let i = (pagina - 1) * cantidad;
        const limite = i + cantidad;
        while(i < limite && i < filas.length) {
            nuevasFilas.push(filas[i]);
            i++;
        }
        return nuevasFilas;
    }

    /**
     *
     * @param filas {*[]}
     * @param busqueda : String
     * @param columnas {*[]}
     * @returns {*[]}
     */
    static getBusquedaFila(filas, busqueda, columnas) {
        let busquedaFila = [];
        if(busqueda.length > 0) {
            for(let i = 0; i < filas.length; i++) {
                let aux = filas[i];
                let incluye = false;
                for(let j = 0; j < columnas.length; j++) {
                    if(String(aux[columnas[j]]).includes(busqueda)){
                        incluye = true;
                        break;
                    }
                }
                if(incluye) {
                    busquedaFila.push(aux);
                }
            }
        } else {
            busquedaFila = filas;
        }
        return busquedaFila;
    }

}