import swal from 'sweetalert';

export default class Alerta {


    static confirmarEliminacion() {
        return swal({
            title: "¿Estas seguro?",
            text: "¡Una vez eliminado, ¡no podrá recuperar esta información!",
            icon: "warning",
            buttons: ['No', 'Si'],
            dangerMode: true,
        }).then((willDelete) => {
            return new Promise( (resolve, reject) => {
               if(willDelete) {
                   resolve();
               } else {
                   reject();
               }
            });
        });
    }


    static alertaError(mensaje) {
        return swal({
            text:mensaje,
            icon:"error",
            button: {
                text: "OK",
                value: true,
                visible: true,
                className: "btn btn-sm btn-success",
                closeModal: true,
            },
        });
    }
}