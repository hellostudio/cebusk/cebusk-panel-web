import { createRouter, createWebHistory } from 'vue-router'
import AppHome from '../AppHome.vue';
import HomeView from '../views/HomeView.vue';
import Preloader from '../views/Preloader.vue';
import Login from '../views/Login.vue';

import SubastaListar from '../views/panel/subasta/Listar.vue';
import SubastaBuscar from '../views/panel/subasta/Buscar.vue';
import SubastaMostrar from '../views/panel/subasta/Mostrar.vue';
import SubastaCrear from '../views/panel/subasta/Crear.vue';
import SubastaEditar from '../views/panel/subasta/Editar.vue';

import TipoGanadoListar from '../views/panel/tipo_ganado/Listar.vue';

import TipoRazaListar from '../views/panel/tipo_raza/Listar.vue';
import TipoAnimalListar from '../views/panel/tipo_animal/Listar.vue';
import CategoriaGanadoListar from '../views/panel/categoria_ganado/Listar.vue';
import AsociacionListar from '../views/panel/asociacion/Listar.vue';

import ProveedorListar from '../views/panel/proveedor/Listar.vue';
import ProveedorMostrar from '../views/panel/proveedor/Mostrar.vue';

import VentaListar from '../views/panel/venta/Listar.vue';
import VentaMostrar from '../views/panel/venta/Mostrar.vue';
import VentaBuscar from '../views/panel/venta/Buscar.vue';

import TransporteListar from '../views/panel/transporte/Listar.vue';
import TransporteBuscar from '../views/panel/transporte/Buscar.vue';
import TransporteMostrar from '../views/panel/transporte/Mostrar.vue';

import UsuarioListar from '../views/panel/usuario/Listar.vue';
import UsuarioMostrar from '../views/panel/usuario/Mostrar.vue';



const routes = [
  {
    path: '/',
    name: 'preloader',
    component: Preloader,
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
  },
  {
    path: '/inicio',
    component: AppHome,
    children: [
      {
        path: '/inicio',
        name: 'inicio',
        components: {
          content: HomeView
        },
        meta: {
          reload: true,
        },
      },
      {
        path: '/panel/tipo-ganado/listar',
        name: 'panel.tipo-ganado.listar',
        components: {
          content: TipoGanadoListar
        }
      },
      {
        path: '/panel/tipo-raza/listar',
        name: 'panel.tipo-raza.listar',
        components: {
          content: TipoRazaListar
        }
      },
      {
        path: '/panel/tipo-animal/listar',
        name: 'panel.tipo-animal.listar',
        components: {
          content: TipoAnimalListar
        }
      },
      {
        path: '/panel/categoria-ganado/listar',
        name: 'panel.categoria-ganado.listar',
        components: {
          content: CategoriaGanadoListar
        }
      },
      {
        path: '/panel/asociacion/listar',
        name: 'panel.asociacion.listar',
        components: {
          content: AsociacionListar
        }
      },
      {
        path: '/panel/proveedor/listar',
        name: 'panel.proveedor.listar',
        components: {
          content: ProveedorListar
        }
      },
      {
        path: '/panel/proveedor/:id/mostrar',
        name: 'panel.proveedor.mostrar',
        components: {
          content: ProveedorMostrar
        }
      },
      {
        path: '/panel/subasta/listar',
        name: 'panel.subasta.listar',
        components: {
          content: SubastaListar
        }
      },
      {
        path: '/panel/subasta/buscar',
        name: 'panel.subasta.buscar',
        components: {
          content: SubastaBuscar
        }
      },
      {
        path: '/panel/subasta/:id/mostrar',
        name: 'panel.subasta.mostrar',
        components: {
          content: SubastaMostrar
        }
      },
      {
        path: '/panel/subasta/crear',
        name: 'panel.subasta.crear',
        components: {
          content: SubastaCrear
        }
      },
      {
        path: '/panel/subasta/:id/editar',
        name: 'panel.subasta.editar',
        components: {
          content: SubastaEditar
        }
      },
      {
        path: '/panel/venta/listar',
        name: 'panel.venta.listar',
        components: {
          content: VentaListar
        }
      },
      {
        path: '/panel/venta/:id/mostrar',
        name: 'panel.venta.mostrar',
        components: {
          content: VentaMostrar
        }
      },
      {
        path: '/panel/venta/buscar',
        name: 'panel.venta.buscar',
        components: {
          content: VentaBuscar
        }
      },
      {
        path: '/panel/transporte/listar',
        name: 'panel.transporte.listar',
        components: {
          content: TransporteListar
        }
      },
      {
        path: '/panel/transporte/buscar',
        name: 'panel.transporte.buscar',
        components: {
          content: TransporteBuscar
        }
      },
      {
        path: '/panel/transporte/:id/mostrar',
        name: 'panel.transporte.mostrar',
        components: {
          content: TransporteMostrar
        }
      },
      {
        path: '/panel/usuario/listar',
        name: 'panel.usuario.listar',
        components: {
          content: UsuarioListar
        }
      },
      {
        path: '/panel/usuario/:id/mostrar',
        name: 'panel.usuario.mostrar',
        components: {
          content: UsuarioMostrar
        }
      },

    ]
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
